window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement('option');
      const conferenceHref = option.value = conference.href;

      console.log("HREF: ", conferenceHref);

      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    // Here, add the 'd-none' class to the loading icon
    const selectSpinnerTag = document.getElementById("loading-conference-spinner");
    selectSpinnerTag.className = "d-flex justify-content-center mb-3 d-none";

    // Here, remove the 'd-none' class from the select tag
    selectTag.className = "form-select";


  }

});

const selectConf = document.getElementById('conference');

selectConf.addEventListener('change', (event) => {
  const urlValue = event.target.value;
  console.log("Value: ",  urlValue);
  // let index = selectElement.selectedIndex;
  // console.log("Name: ",  selectElement.options[index].innerHTML);

// below: code to create new form / POST

window.addEventListener('submit', async (event) => {

  event.preventDefault();
  console.log("Need to submit the form data");

  const form = document.getElementById("create-attendee-form");

  const formData = new FormData(form);

  const json = JSON.stringify(Object.fromEntries(formData));
  console.log("JSON: ", json);

  const conferenceUrl = `http://localhost:8001/${urlValue}attendees/`;

  const fetchConfig = {
    method: "post",
    body: json,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await fetch(conferenceUrl, fetchConfig);
  if (response.ok) {
    form.reset();
    const newAttendee = await response.json();
    console.log(newAttendee);
  }

});

});
