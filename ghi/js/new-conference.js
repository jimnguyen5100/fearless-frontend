window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    console.log("url:", url);

    const response = await fetch(url);


    if (!response.ok) {

      console.log("error response not ok");
      throw error;
    }
    else {
      const data = await response.json();
      const locations = data.locations;
      for (let location of locations) {

        let pk = location.id;
        let locationName = location.name;
        let selectTag = document.getElementById("location");
        let option = document.createElement("option");

        option.value = pk;
        option.innerHTML = locationName;

        selectTag.appendChild(option);

      }
    }

  });

  const form = document.getElementById("create-location-form");

  form.addEventListener("submit", formSubmit);

  async function formSubmit(event) {
    event.preventDefault();
    console.log("Need to submit the form data");

    const formData = new FormData(form);

    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      form.reset();
      const newConference = await response.json();
      console.log(newConference);
    }

  }
