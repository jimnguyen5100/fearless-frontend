window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/states/';
  console.log("url:", url);

  const response = await fetch(url);


  if (!response.ok) {

    console.log("error response not ok");
    throw error;
  }
  else {

    const data = await response.json();
    const states_list = data.states;

    for (let state of states_list) {

      let stateName = state.name;
      let stateAbbrev = state.abbreviation;
      let selectTag = document.getElementById("state");
      let option = document.createElement("option");
      option.value = stateAbbrev;
      option.innerHTML = stateName;
      selectTag.appendChild(option);
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {

      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
      }
    });


  }

});
