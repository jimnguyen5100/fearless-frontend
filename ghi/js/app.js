function createCard(title, description, pictureUrl, start, end, location) {
  return `
    <div class="card shadow-sm p-3 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${start} - ${end}
    </div>
  `;
}

function createMessage(){
  return `
  <div class="alert alert-danger d-flex align-items-center" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
    <div>"We're sorry this page has not been found."
    </div>
  </div>`;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
  const response = await fetch(url);

  if (!response.ok) {

    console.log("error response not ok");
    throw error;
  }
  else {
    const data = await response.json();

    let index = 0;

    for (let conference of data.conferences) {

      // console.log("conference: ", conference);

      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);

      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const startDate = details.conference.starts;

        const strt = new Date(startDate);
        const startDay = strt.getDate();
        const startMonth = strt.getMonth() + 1;
        const startYear = strt.getFullYear();
        // console.log(startMonth + "/" + startDay + "/" + startYear);
        const start = startMonth + "/" + startDay + "/" + startYear;

        const endDate = details.conference.ends;
        const ends = new Date(endDate);
        const endDay = ends.getDate();
        const endMonth = ends.getMonth() + 1;
        const endYear = ends.getFullYear();
        // console.log(endMonth + "/" + endDay + "/" + endYear);
        const end = endMonth + "/" + endDay + "/" + endYear;

        const location = details.conference.location.name;

        const html = createCard(title, description, pictureUrl, start, end, location);
        // console.log("html: ", html);
        console.log("detail: ", details);

        const column = document.querySelector(`#col-${index % 3}`);
        column.innerHTML += html;

        index += 1;

      }
    }
  }
}
catch (e) {

  const alert = createMessage()
  const body = document.querySelector('body');

  body.innerHTML = alert;
  console.log("error catch")
}
});

